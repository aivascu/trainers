#include "stdafx.h"
#include "Scanner.h"

using namespace ucl;
using namespace std;

Scanner::Scanner(wstring exeName)
{
    processName = exeName;
    processId = this->GetProcessId();
    this->GetProcessHandle();
}

Scanner::~Scanner()
{
    CloseHandle(processHandle);
}

int Scanner::ReadInt(DWORD address)
{
    int value;
    ReadProcessMemory(processHandle, (LPVOID)address, &value, sizeof(value), NULL);
    return value;
}

float Scanner::ReadFloat(DWORD address)
{
    float value;
    ReadProcessMemory(processHandle, (LPVOID)address, &value, sizeof(value), NULL);
    return value;
}

double Scanner::ReadDouble(DWORD address)
{
    double value;
    ReadProcessMemory(processHandle, (LPVOID)address, &value, sizeof(value), NULL);
    return value;
}

bool Scanner::WriteInt(DWORD address, int value)
{
    DWORD dataSize = sizeof(value);
    return WriteProcessMemory(processHandle, (LPVOID)address, &value, dataSize, NULL);
}

bool Scanner::WriteFloat(DWORD address, float value)
{
    DWORD dataSize = sizeof(value);
    return WriteProcessMemory(processHandle, (LPVOID)address, &value, dataSize, NULL);
}

bool Scanner::WriteDouble(DWORD address, double value)
{
    DWORD dataSize = sizeof(value);
    return WriteProcessMemory(processHandle, (LPVOID)address, &value, dataSize, NULL);
}

void * Scanner::Scan(string pattern, string mask)
{
    return this->Scan(processName, pattern, mask);
}

void * Scanner::Scan(wstring module, string pattern, string mask)
{
    MODULEENTRY32 modEntry = this->GetModuleEntry(module);
    uintptr_t begin = (uintptr_t)modEntry.modBaseAddr;
    uintptr_t end = begin + modEntry.modBaseSize;
    return this->ScanMemoryRange(begin, end, pattern, mask);
}

void Scanner::Patch(void * destination, void * source, unsigned int size)
{
    DWORD oldMemoryProtection;
    VirtualProtectEx(processHandle, destination, size, PAGE_READWRITE, &oldMemoryProtection);
    WriteProcessMemory(processHandle, destination, source, size, NULL);
    VirtualProtectEx(processHandle, destination, size, oldMemoryProtection, &oldMemoryProtection);
}

void ucl::Scanner::Nop(void * destination, unsigned int size)
{
    auto nopArray = new byte[size];

    memset(nopArray, 0x90, size);
    this->Patch(destination, nopArray, size);

    delete[] nopArray;
}

DWORD ucl::Scanner::GetPID()
{
    return processId;
}

HANDLE Scanner::GetProcessHandle()
{
    if (processId == 0)
        return 0;

    processHandle = OpenProcess(PROCESS_ALL_ACCESS, false, processId);
    return processHandle;
}

void * Scanner::ScanMemoryPage(char * base, size_t size, string pattern, string mask)
{
    size_t patternLength = pattern.length();

    for (unsigned int i = 0; i < size - patternLength; i++)
    {
        bool found = true;
        for (unsigned int j = 0; j < patternLength; j++)
        {
            if (mask[j] != '?' && pattern[j] != *(base + i + j))
            {
                found = false;
                break;
            }
        }

        if (found)
            return (void*)(base + i);
    }

    return nullptr;
}

void * Scanner::ScanMemoryRange(uintptr_t begin, uintptr_t end, string pattern, string mask)
{
    uintptr_t currentChunk = begin;
    SIZE_T bytesRead;

    while (currentChunk < end)
    {
        char buffer[4096];

        DWORD oldprotect;
        VirtualProtectEx(processHandle, (void*)currentChunk, sizeof(buffer), PROCESS_VM_READ, &oldprotect);
        ReadProcessMemory(processHandle, (void*)currentChunk, &buffer, sizeof(buffer), &bytesRead);
        VirtualProtectEx(processHandle, (void*)currentChunk, sizeof(buffer), oldprotect, NULL);

        if (bytesRead == 0)
            return nullptr;

        void* internalAddress = this->ScanMemoryPage((char*)&buffer, bytesRead, pattern, mask);

        if (internalAddress != nullptr)
        {
            //calculate from internal to external
            uintptr_t offsetFromBuffer = (uintptr_t)internalAddress - (uintptr_t)&buffer;
            return (void*)(currentChunk + offsetFromBuffer);
        }

        //advance to next chunk
        currentChunk = currentChunk + bytesRead;
    }
    return nullptr;
}

DWORD Scanner::GetProcessId()
{
    PROCESSENTRY32 processEntry = { 0 };
    auto snapshotHandle = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, NULL);

    if (!snapshotHandle)
        return 0;

    processEntry.dwSize = sizeof(processEntry);

    if (!Process32First(snapshotHandle, &processEntry))
        return 0;

    do
    {
        if (!wcscmp(processEntry.szExeFile, processName.c_str()))
        {
            CloseHandle(snapshotHandle);
            return processEntry.th32ProcessID;
        }
    } while (Process32Next(snapshotHandle, &processEntry));

    CloseHandle(snapshotHandle);
    return 0;
}

MODULEENTRY32 Scanner::GetModuleEntry(wstring moduleName)
{
    MODULEENTRY32 moduleEntry = { 0 };

    auto snapshot = CreateToolhelp32Snapshot(TH32CS_SNAPMODULE | TH32CS_SNAPMODULE32, processId);

    if (snapshot == INVALID_HANDLE_VALUE)
        return moduleEntry;

    moduleEntry.dwSize = sizeof(MODULEENTRY32);

    if (Module32First(snapshot, &moduleEntry))
    {
        do
        {
            if (!wcscmp(moduleEntry.szModule, moduleName.c_str()))
                break;
        } while (Module32Next(snapshot, &moduleEntry));
    }

    CloseHandle(snapshot);
    return moduleEntry;
}