#pragma once

namespace ucl {
    class Scanner
    {
    public:
        Scanner(std::wstring exeName);
        ~Scanner();

        ///<summary>
        /// Reads an Int32 value from the specified address
        ///</summary>
        int ReadInt(DWORD address);

        ///<summary>
        /// Reads a Float value from the specified address
        ///</summary>
        float ReadFloat(DWORD address);

        ///<summary>
        /// Reads a Double value from the specified address
        ///</summary>
        double ReadDouble(DWORD address);

        bool WriteInt(DWORD address, int value);

        bool WriteFloat(DWORD address, float value);

        bool WriteDouble(DWORD address, double value);

        ///<summary>
        /// Scans the current module for a pattern and retruns the address
        ///</summary>
        void* Scan(std::string pattern, std::string mask);

        ///<summary>
        /// Scans the specified module for a pattern and retruns the address
        ///</summary>
        void* Scan(std::wstring module, std::string pattern, std::string mask);

        void Patch(void* destination, void* source, unsigned int size);

        void Nop(void* destination, unsigned int size);

        DWORD GetPID();

    private:
        ///<summary>
        /// The current process Id
        ///</summary>
        DWORD processId;

        ///<summary>
        /// The current process handle
        ///</summary>
        HANDLE processHandle;

        std::wstring processName;

        ///<summary>
        /// Returns the Process Id
        ///</summary>
        DWORD GetProcessId();

        ///<summary>
        /// Returns the module entry point
        ///</summary>
        MODULEENTRY32 GetModuleEntry(std::wstring moduleName);

        ///<summary>
        /// Opens a window handle and returns it
        ///</summary>
        HANDLE GetProcessHandle();

        void* ScanMemoryPage(char* base, size_t size, std::string pattern, std::string mask);

        void* ScanMemoryRange(uintptr_t begin, uintptr_t end, std::string pattern, std::string  mask);
    };
};
