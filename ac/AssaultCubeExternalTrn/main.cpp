// AssaultCubeExternalTrn.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

using namespace ucl;
using namespace std;

wstring processName = L"Tutorial-i386.exe";
DWORD intAddr = 0x01770BC8;
DWORD floatAddr = 0x0177427C;
DWORD doubleAddr = 0x01774280;
DWORD processId;
int value;

string pattern = "\x89\x83\x80\x04\x00\x00\x8D\x55\xD4\xE8\x02";
string mask = "xxxxxxxxxxx";

int main()
{
    Scanner scanner(processName);

    auto PID = scanner.GetPID();

    if (PID != 0)
        cout << "Process found: " << hex << PID << endl;
    else
    {
        cout << "Error: Process could not be found!" << endl;
        return 0;
    }

    auto codeAddress = scanner.Scan(pattern, mask);
    scanner.Nop(codeAddress, mask.size());

    auto floatValue = scanner.ReadFloat(floatAddr);
    cout << floatValue << endl;

    scanner.WriteFloat(floatAddr, 500.0f);
    floatValue = scanner.ReadFloat(floatAddr);
    cout << floatValue << endl;

    cin.get();
    return 0;
}